#Fontconvert

#简介
简繁火星文转换是一款简、繁、火星文字体转换的软件。输入要转换的内容点击简体则显示简体，点击繁体则显示繁体，点击火星文则显示火星文，方便实用。

#演示
http://www.wandoujia.com/apps/com.mj.fontconvert

#捐赠
开源，我们是认真的，感谢您对我们开源力量的鼓励。


![支付宝](https://git.oschina.net/uploads/images/2017/0607/164544_ef822cc0_395618.png "感谢您对我们开源力量的鼓励")
![微信](https://git.oschina.net/uploads/images/2017/0607/164843_878b9b7f_395618.png "感谢您对我们开源力量的鼓励")