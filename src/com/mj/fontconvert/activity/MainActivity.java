package com.mj.fontconvert.activity;

import java.util.Timer;
import java.util.TimerTask;

import com.baidu.appx.BDBannerAd;
import com.baidu.appx.BDBannerAd.BannerAdListener;
import com.baidu.appx.BDInterstitialAd;
import com.baidu.appx.BDInterstitialAd.InterstitialAdListener;
import com.mj.fontconvert.R;
import com.mj.fontconvert.util.FontconvertUtil;

import android.app.Activity;
import android.os.Bundle;
import android.text.ClipboardManager;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
/**
 * 简繁火星文转换
 * @author zhaominglei
 * @date 2015-3-31
 * 
 */
public class MainActivity extends Activity implements OnClickListener{
	private static final String TAG = MainActivity.class.getSimpleName();
	private boolean isExit = false;
	private TimerTask timerTask;
	private EditText fontconvertText; //要转换的文本输入框
	private String targetText; //要转换的文本
	private Button jtBtn; //简体
	private Button ftBtn; //繁体
	private Button hxBtn; //火星文
	private Button copyBtn; //复制结果
	private TextView resultView; //结果
	private String result; //结果
	private Button appOffersButton; //推荐应用
	private RelativeLayout appxBannerContainer; //appx 容器
	private static BDBannerAd bannerAdView; //横幅广告
	private BDInterstitialAd appxInterstitialAdView; //插屏广告
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.activity_main);
		init();
	}

	private void init() {
		fontconvertText = (EditText)findViewById(R.id.fontconvert_text_edt);
		jtBtn = (Button)findViewById(R.id.fontconvert_jt_btn);
		ftBtn = (Button)findViewById(R.id.fontconvert_ft_btn);
		hxBtn = (Button)findViewById(R.id.fontconvert_hx_btn);
		copyBtn = (Button)findViewById(R.id.fontconvert_copy_btn);
		resultView = (TextView)findViewById(R.id.fontconvert_result);
		appOffersButton = (Button)findViewById(R.id.appOffersButton);
		
		jtBtn.setOnClickListener(this);
		ftBtn.setOnClickListener(this);
		hxBtn.setOnClickListener(this);
		copyBtn.setOnClickListener(this);
		appOffersButton.setOnClickListener(this);
		
		//创建广告视图 发布时请使用正确的ApiKey和广告位ID
		bannerAdView = new BDBannerAd(this, "GdpYnVXVpbIQRux9vLlK0wr2",
				"98WFBv3mQxe6WR0oobQzixBr");
		//设置横幅广告展示尺寸，如不设置，默认为SIZE_FLEXIBLE;
//				bannerAdView.setAdSize(BDBannerAd.SIZE_320X50);
		//设置横幅广告行为监听器
		bannerAdView.setAdListener(new BannerAdListener() {
			@Override
			public void onAdvertisementDataDidLoadFailure() {
				Log.e(TAG, "load failure");
			}
			@Override
			public void onAdvertisementDataDidLoadSuccess() {
				Log.e(TAG, "load success");
			}
			@Override
			public void onAdvertisementViewDidClick() {
				Log.e(TAG, "on click");
			}
			@Override
			public void onAdvertisementViewDidShow() {
				Log.e(TAG, "on show");
			}
			@Override
			public void onAdvertisementViewWillStartNewIntent() {
				Log.e(TAG, "leave app");
			}
		});
		appxBannerContainer = (RelativeLayout) findViewById(R.id.appx_banner_container);
		appxBannerContainer.addView(bannerAdView);
		
		//创建广告视图 发布时请使用正确的ApiKey和广告位ID
		appxInterstitialAdView = new BDInterstitialAd(this,
				"GdpYnVXVpbIQRux9vLlK0wr2", "Keo6LI6mqTCZAO1VhUmrVMnp");
		//设置插屏广告行为监听器
		appxInterstitialAdView.setAdListener(new InterstitialAdListener() {
			@Override
			public void onAdvertisementDataDidLoadFailure() {
				Log.e(TAG, "load failure");
			}
			@Override
			public void onAdvertisementDataDidLoadSuccess() {
				Log.e(TAG, "load success");
			}
			@Override
			public void onAdvertisementViewDidClick() {
				Log.e(TAG, "on click");
			}
			@Override
			public void onAdvertisementViewDidHide() {
				Log.e(TAG, "on hide");
			}
			@Override
			public void onAdvertisementViewDidShow() {
				Log.e(TAG, "on show");
			}
			@Override
			public void onAdvertisementViewWillStartNewIntent() {
				Log.e(TAG, "leave");
			}
		});
		//加载广告
		appxInterstitialAdView.loadAd();
	}

	@Override
	public void onClick(View view) {
		switch (view.getId()) {
		case R.id.fontconvert_jt_btn:
			fontconvert("jt");
			break;
			
		case R.id.fontconvert_ft_btn:
			fontconvert("ft");	
			break;
			
		case R.id.fontconvert_hx_btn:
			fontconvert("hx");
			break;
	
		case R.id.fontconvert_copy_btn:
			targetText = fontconvertText.getText().toString();
			if (targetText == null || targetText.equals("")) {
				Toast.makeText(getApplicationContext(), R.string.fontconvert_text_hint, Toast.LENGTH_SHORT).show();
				return;
			}
			if (result != null && !result.equals("")) {
				ClipboardManager clipboardManager = (ClipboardManager)getSystemService(CLIPBOARD_SERVICE);
				clipboardManager.setText(result);
				Toast.makeText(getApplicationContext(), R.string.fontconvert_copy_success, Toast.LENGTH_SHORT).show();
			}
			break;
			
		case R.id.appOffersButton:
			//展示插屏广告前先请先检查下广告是否加载完毕
			if (appxInterstitialAdView.isLoaded()) {
				appxInterstitialAdView.showAd();
			} else {
				Log.i(TAG, "AppX Interstitial Ad is not ready");
				appxInterstitialAdView.loadAd();
			}
			break;

		default:
			break;
		}
	}
	
	private void fontconvert(String type) {
		targetText = fontconvertText.getText().toString();
		if (targetText == null || targetText.equals("")) {
			Toast.makeText(getApplicationContext(), R.string.fontconvert_text_hint, Toast.LENGTH_SHORT).show();
			return;
		}
		result = "";
		if ("jt".equals(type)) {
			result = FontconvertUtil.jtgo(targetText);
		} else if ("ft".equals(type)) {
			result = FontconvertUtil.ftgo(targetText);
		} else if ("hx".equals(type)) {
			result = FontconvertUtil.hxgo(targetText);
		} else {
			result = targetText;
		}
		
		resultView.setText(result);
	}

	@Override
	public void onBackPressed() {
		if (isExit) {
			MainActivity.this.finish();
		} else {
			isExit = true;
			Toast.makeText(MainActivity.this, R.string.exit_msg, Toast.LENGTH_SHORT).show();
			timerTask = new TimerTask() {
				@Override
				public void run() {
					isExit = false;
				}
			};
			new Timer().schedule(timerTask, 2*1000);
		}
	}
}
